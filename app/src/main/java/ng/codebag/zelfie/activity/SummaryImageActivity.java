package ng.codebag.zelfie.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import ng.codebag.zelfie.R;

/**
 * Created by ndu on 9/15/17.
 */

public class SummaryImageActivity extends AppCompatActivity {
    Bitmap photo  = null;
    ImageView imageView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary_image_layout);

        Bundle extras = getIntent().getExtras();


        Bitmap photos =(Bitmap) extras.get("image");
        imageView = (ImageView) findViewById(R.id.testImageView);
        imageView.setImageBitmap(photos);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1){
            photo = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(photo);
        }
    }
}
