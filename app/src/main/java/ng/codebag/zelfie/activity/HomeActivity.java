package ng.codebag.zelfie.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.List;

import ng.codebag.zelfie.R;

import static android.content.Context.WIFI_SERVICE;

public class HomeActivity extends AppCompatActivity {
    public static final String TAG = "Zelfie app";

    //Initialize the View groups
    private ImageButton cameraButton;
    private ImageButton socialButton;
    private ImageButton joinConnectionButton;
    private ImageButton openConnectionButton;
    //End to initializing the view groups

    //initialize the camerarequest to be 1
    public static final int CAMERA_REQUEST = 1;

    //Initialize Wifi Connectors
    WifiManager wifiManager;
    WifiInfo wifiInfo;


    //initializing classes am going to use this activity

    public Dialog socialDialog;

    public Bitmap photo;

    public List<Bitmap> photos;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Log.d(TAG,"OnCreate Called");

        //Make a link to my home layout with the declared objects
        cameraButton = (ImageButton) findViewById(R.id.camera_image_button);
        socialButton = (ImageButton) findViewById(R.id.social_image_button);
        joinConnectionButton = (ImageButton) findViewById(R.id.join_wifi_image_view);
        openConnectionButton = (ImageButton) findViewById(R.id.setup_hotspot_image_button);
        //End of link to the layout

        //Declaring initialize Wifi connectors
        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);

        wifiInfo = (WifiInfo) wifiManager.getConnectionInfo();


        //Create a click listener for each of the button
        /**
         * The camera button should open up the camera to take pictures by calling the handleCamera method
         */
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleCamera();
            }
        });
        //End of click listener for camera button
        /**
         * The social button should open up the user's social account especially whatsapp
         */
        socialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleSocial();
            }
        });
        //End of click listener method for social button

        /**
         * The join method will open the wifi connection for user to connect to available zelfie connection
         */
        joinConnectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleWifiConnection();
            }
        });
        //End of wifi join connection button click listener event

        /**
         * The open Connection assess the mobile hotspot for data transfer
         */
        openConnectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleHotspot();
            }
        });
        //End of hotspot openning event listener
    }

    /**
     * This handles the event for social clicked
     */
    private void handleSocial() {

    }

    /**
     * This method handles Hotspot creation for file transfer
     */
    private void handleHotspot() {

    }

    /**
     * This class handles the wifi connection for available zelfie hotspots
     */
    private void handleWifiConnection() {
        changeWifiState();
    }

    /**
     * This class handles the camera action when clicked from the home view
     */
    private void handleCamera() {
        //Create an intent that opens the camera and start the activity
        Intent cameraIntent = new Intent(MediaStore.INTENT_ACTION_VIDEO_CAMERA);
        startActivityForResult(cameraIntent,CAMERA_REQUEST);

//        Intent imgSummaryIntent = new Intent(getApplicationContext(),SummaryImageActivity.class);
//        startActivity(imgSummaryIntent);
    }

    /**
     * This method handles the camera click activity result, what happens
     * after using the camera, takes three parameters
     * @param requestCode takes the request which could be 0 or 1
     * @param resultCode takes the result code
     * @param data takes the data which is the picture here
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /**
         * Write an asynchTask class that sends the image into the list of photos while the user can take more pictures
         */
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK){
            Log.d(TAG,"Camera on");
            photo = (Bitmap) data.getExtras().get("data");
            //photos.add(photo);
            Intent imgSummaryIntent = new Intent();
            imgSummaryIntent.putExtra("data",photo);
            setResult(1,imgSummaryIntent);
            finish();

        }
    }
    //Starting point for handling the wifi application

    public void changeWifiState(){
        Log.d(TAG, "Want to change Wifi State");
        if (!wifiManager.isWifiEnabled()){
            wifiManager.setWifiEnabled(true);
            String ssid = wifiInfo.getSSID();
            String wifiName = wifiInfo.toString();

            //Notifiers
            Toast.makeText(getApplicationContext(),"Wifi Enabled file transfer "+wifiName+" \n"+ssid,Toast.LENGTH_LONG).show();
            Log.d(TAG,"Wifi state:On");
        }else {
            wifiManager.setWifiEnabled(false);
            Toast.makeText(getApplicationContext(),"Wifi Disabled ",Toast.LENGTH_SHORT).show();
            Log.d(TAG,"Wifi state:Off");
        }

    }

}
