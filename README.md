Zelfie
This is an android application that aids social interraction. With this app a user snaps a picture and all his friends present in the picture gets the same picture instantly. This solves the problem of having to figure out the pictures for each friend.

How it Works- Case Study
Provided in a party i have the best camera pixel, i will take the honour to snap all my friends, before or after snapping i will setup a zelfi connection using hotspot while my friends on the other end join the connection. While snapping they get a copy of the picture.

Installing and Getting Started.
I will assume installation of android studio, preferably the new version, create a new project, name it anything but start with an uppercase, in this case Zelfie. Select the empty activity,name it HomeActivity and this also creates a home layout for you, wait for the project to be gradled and a new working space is created for you to work with.
	You will be presented with the HomeActivity and the home layout as the starting point for your project.
